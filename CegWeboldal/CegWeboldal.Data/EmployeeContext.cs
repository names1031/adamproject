﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CegWeboldal.Data
{
    public partial class EmployeeContext : DbContext
    {
        // Scaffold-DbContext "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\EmployeeDatabase.mdf;Integrated Security=True" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models

        public EmployeeContext()
        {
        }

        public EmployeeContext(DbContextOptions<EmployeeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Employee> Employees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.
                    UseLazyLoadingProxies().
                    UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\EmployeeDatabase.mdf;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Empid)
                    .HasName("EMP_PRIMARY_KEY");

                entity.ToTable("EMPLOYEE");

                entity.Property(e => e.Empid)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("EMPID");

                entity.Property(e => e.Cv)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("CV");

                entity.Property(e => e.Emppassword)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("EMPPASSWORD");

                entity.Property(e => e.Fname)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("FNAME");

                entity.Property(e => e.Manager)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("MANAGER");

                entity.Property(e => e.Picture)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("PICTURE");

                entity.Property(e => e.Post)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("POST");

                entity.Property(e => e.Sname)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("SNAME");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("USERNAME");

                entity.HasOne(d => d.ManagerNavigation)
                    .WithMany(p => p.InverseManagerNavigation)
                    .HasForeignKey(d => d.Manager)
                    .HasConstraintName("EMP_BOSS_KEY");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
