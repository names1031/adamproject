﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CegWeboldal.Data
{
    public partial class Employee
    {
        public Employee()
        {
            InverseManagerNavigation = new HashSet<Employee>();
        }

        public decimal Empid { get; set; }
        public string Fname { get; set; }
        public string Sname { get; set; }
        public decimal? Manager { get; set; }
        public string Post { get; set; }
        public string Picture { get; set; }
        public string Cv { get; set; }
        public string Emppassword { get; set; }
        public string Username { get; set; }
        public virtual Employee ManagerNavigation { get; set; }
        public virtual ICollection<Employee> InverseManagerNavigation { get; set; }
    }
}
