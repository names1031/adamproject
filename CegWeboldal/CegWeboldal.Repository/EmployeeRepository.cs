﻿using CegWeboldal.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Repository
{
    public class EmployeeRepository : ElderRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext ctx)
            : base(ctx)
        {
        }

        public void UpdateEmployee(decimal id, string newCV, string newFname, string newSname, string newPost, string newPicture)
        {
            var employee = this.GetOne(id);
            employee.Cv = newCV;
            employee.Fname = newFname;
            employee.Sname = newSname;
            employee.Post = newPost;
            employee.Picture = newPicture;
            this.Ctx.SaveChanges();
        }

        public override Employee GetOne(decimal id)
        {
            return this.GetAll().SingleOrDefault(x => x.Empid == id);
        }
    }
}
