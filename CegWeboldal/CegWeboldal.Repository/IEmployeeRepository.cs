﻿using CegWeboldal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Repository
{
    public interface IEmployeeRepository : IElderRepository<Employee>
    {
        void UpdateEmployee(decimal id, string newCV, string newFname, string newSname, string newPost, string newPicture);
    }
}
