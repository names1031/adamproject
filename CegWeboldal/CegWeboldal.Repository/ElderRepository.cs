﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CegWeboldal.Repository
{
    public abstract class ElderRepository<T> : IElderRepository<T>
        where T : class
    {
        protected ElderRepository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        public DbContext Ctx { get; set; }

        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        public abstract T GetOne(decimal id);

        public void Insert(T entity)
        {
            this.Ctx.Set<T>().Add(entity);
            this.Ctx.SaveChanges();
        }

        public void Remove(T entity)
        {
            this.Ctx.Set<T>().Remove(entity);
            this.Ctx.SaveChanges();
        }
    }
}
