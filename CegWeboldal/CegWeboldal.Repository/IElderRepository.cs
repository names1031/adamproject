﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Repository
{
    public interface IElderRepository<T>
        where T : class
    {
        T GetOne(decimal id);

        IQueryable<T> GetAll();

        void Insert(T entity);

        void Remove(T entity);
    }
}
