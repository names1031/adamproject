﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CegWeboldal.Models
{
    public class EmployeeListViewModel
    {
        public List<Employee> Employees { get; set; }
        public Employee EditedEmployee { get; set; }
    }
}
