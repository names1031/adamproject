﻿using AutoMapper;

namespace CegWeboldal.Models
{

    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Employee, Models.Employee>()
                .ForMember(dest => dest.Empid, map => map.MapFrom(src => src.Empid))
                .ForMember(dest => dest.Fname, map => map.MapFrom(src => src.Fname))
                .ForMember(dest => dest.Sname, map => map.MapFrom(src => src.Sname))
                .ForMember(dest => dest.Manager, map => map.MapFrom(src => src.Manager))
                .ForMember(dest => dest.Post, map => map.MapFrom(src => src.Post))
                .ForMember(dest => dest.Picture, map => map.MapFrom(src => src.Picture))
                .ForMember(dest => dest.Cv, map => map.MapFrom(src => src.Cv))
                .ForMember(dest => dest.Emppassword, map => map.MapFrom(src => src.Emppassword))
                .ForMember(dest => dest.Username, map => map.MapFrom(src => src.Username))
                /*.ReverseMap()*/;
            });

            return config.CreateMapper();
        }
    }
}
