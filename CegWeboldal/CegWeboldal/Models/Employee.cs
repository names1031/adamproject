﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace CegWeboldal.Models
{
    public partial class Employee
    {
        [Required]
        [Display(Name = "Employee id: ")]
        public decimal Empid { get; set; }

        [Required]
        [Display(Name = "First name: ")]
        public string Fname { get; set; }

        [Required]
        [Display(Name = "Last name: ")]
        public string Sname { get; set; }

        [Display(Name = "Manager: ")]
        public decimal? Manager { get; set; }

        [Required]
        [Display(Name = "Post: ") ]
        public string Post { get; set; }
        public string Picture { get; set; }
        public string Cv { get; set; }
        [Display(Name = "Password")]
        public string Emppassword { get; set; }
        public string Username { get; set; }

    }
}
