#pragma checksum "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\Employees\EmployeesIndex.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1f8a59f47b2d1aeae637062d54ae68d4b0a1b8cb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Employees_EmployeesIndex), @"mvc.1.0.view", @"/Views/Employees/EmployeesIndex.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\_ViewImports.cshtml"
using CegWeboldal;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\_ViewImports.cshtml"
using CegWeboldal.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1f8a59f47b2d1aeae637062d54ae68d4b0a1b8cb", @"/Views/Employees/EmployeesIndex.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"53226f4bc3a41e7c9d12475d7fa327c9f2320548", @"/Views/_ViewImports.cshtml")]
    public class Views_Employees_EmployeesIndex : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<CegWeboldal.Models.EmployeeListViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\Employees\EmployeesIndex.cshtml"
  
    ViewData["Title"] = "EmployeesIndex";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<p style=\"color:red\">");
#nullable restore
#line 7 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\Employees\EmployeesIndex.cshtml"
                Write(TempData["editResult"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n\r\n");
#nullable restore
#line 9 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\Employees\EmployeesIndex.cshtml"
Write(await Html.PartialAsync("EmployeesLogin", Model.EditedEmployee));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 11 "C:\Users\NamesánszkiDávidMárk\source\repos\adamproject\CegWeboldal\CegWeboldal\Views\Employees\EmployeesIndex.cshtml"
Write(await Html.PartialAsync("EmployeesList", Model.Employees));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<CegWeboldal.Models.EmployeeListViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
