﻿using AutoMapper;
using CegWeboldal.Logic;
using CegWeboldal.Models;
using Microsoft.AspNetCore.Mvc;
using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CegWeboldal.Controllers
{
    public class EmployeesController : Controller
    {
        private ICrudFunctions crudFunctions;
        private ILoginLogic loginLogic;
        private IMapper mapper;
        private EmployeeListViewModel vm;

        public EmployeesController(ICrudFunctions crudFunctions, ILoginLogic loginLogic, IMapper mapper)
        {
            this.crudFunctions = crudFunctions;
            this.loginLogic = loginLogic;
            this.mapper = mapper;
            this.vm = new EmployeeListViewModel();
            this.vm.EditedEmployee = new Models.Employee();
            var employees = this.crudFunctions.GetAllEmployees();
            this.vm.Employees = this.mapper.Map<IList<Data.Employee>, List<Models.Employee>>(employees);
        }

        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("EmployeesIndex", this.vm);
        }

        public IActionResult Details(decimal id)
        {
            return this.View("EmployeesDetails", this.GetEmployeeModel(id));
        }

        [HttpPost]
        public IActionResult Edit(Models.Employee employee, string editAction)
        {
            if (this.ModelState.IsValid && employee != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        this.crudFunctions.InsertEmployee(employee.Fname, employee.Sname, employee.Post);
                    }
                    catch (ArgumentException ex)
                    {
                        this.TempData["editResult"] = "AddEmployee FAIL: " + ex.Message;
                    }
                }
                else
                {
                    this.crudFunctions.UpdateEmployee(employee.Empid, employee.Cv, employee.Fname, employee.Sname, employee.Post, employee.Picture);
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedEmployee = employee;
                return this.View("EmployeesIndex", this.vm);
            }
        }

        public IActionResult Remove(decimal id)
        {
            this.TempData["editResult"] = "Delete OK";
            this.crudFunctions.RemoveEmployee(id);
            return this.RedirectToAction(nameof(this.Index));
        }

        public IActionResult Edit(decimal id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedEmployee = this.GetEmployeeModel(id);
            return this.View("EmployeesIndex", this.vm);
        }

        [HttpPost]
        public IActionResult Login()
        {
            if (loginLogic.LoginCheck(this.Request.Form.ToList()[0].Value, this.Request.Form.ToList()[1].Value))
            {
                return this.View("EmployeesDetails", this.GetEmployeeModel(1));
            }
            return this.RedirectToAction(nameof(this.Index));
        }

        private Models.Employee GetEmployeeModel(decimal id)
        {
            Data.Employee oneEmployee = this.crudFunctions.GetEmployeeById(id);
            return this.mapper.Map<Data.Employee, Models.Employee>(oneEmployee);
        }
    }
}
