﻿using CegWeboldal.Data;
using CegWeboldal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CegWeboldal.Logic
{
    public class CrudFunctions : ICrudFunctions
    {
        private readonly IEmployeeRepository empRepo;

        public CrudFunctions(IEmployeeRepository empRepo)
        {
            this.empRepo = empRepo;
        }

        public void InsertEmployee(string fName, string sName, string post)
        {
            Employee newEmp = new Employee();
            newEmp.Fname = fName;
            newEmp.Sname = sName;
            newEmp.Post = post;

            this.empRepo.Insert(newEmp);
        }

        public void RemoveEmployee(decimal id)
        {
            this.empRepo.Remove(this.empRepo.GetOne(id));
        }

        public IList<Employee> GetAllEmployees()
        {
            return this.empRepo.GetAll().ToList();
        }

        public Employee GetEmployeeById(decimal id)
        {
            return this.empRepo.GetOne(id);
        }

        public void UpdateEmployee(decimal id, string newCv, string newFname, string newSname, string newPost, string newPicture)
        {
            this.empRepo.UpdateEmployee(id, newCv, newFname, newSname, newPost, newPicture);
        }
    }
}
