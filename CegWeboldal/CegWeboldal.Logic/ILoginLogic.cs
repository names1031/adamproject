﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Logic
{
    public interface ILoginLogic
    {
        bool LoginCheck(string username, string password);
    }
}
