﻿using CegWeboldal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Logic
{
    public class LoginLogic : ILoginLogic
    {
        private readonly IEmployeeRepository empRepo;

        public LoginLogic(IEmployeeRepository empRepo)
        {
            this.empRepo = empRepo;
        }

        public bool LoginCheck(string username, string password)
        {
            return empRepo.GetAll().Where(x => x.Username == username && x.Emppassword == password).FirstOrDefault() != null;
        }
    }
}
