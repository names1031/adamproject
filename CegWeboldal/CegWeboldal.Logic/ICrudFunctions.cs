﻿using CegWeboldal.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CegWeboldal.Logic
{
    public interface ICrudFunctions
    {
        void InsertEmployee(string fName, string sName, string post);
        public void RemoveEmployee(decimal id);
        public IList<Employee> GetAllEmployees();
        public Employee GetEmployeeById(decimal id);
        public void UpdateEmployee(decimal id, string newCv, string newFname, string newSname, string newPost, string newPicture);

    }
}
